import React from "react";
import Layout from "./components/Layout";
import "./App.css";
import Home from "./screens/Home";

function App() {
  return (
    <Layout>
      <Home />
    </Layout>
  );
}

export default App;
