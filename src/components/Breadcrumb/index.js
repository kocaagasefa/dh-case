import React from "react";
import Container from "../Common/Container";
import Icon from "../Common/Icon";
import classes from "./style.module.css";

const Breadcrumb = () => {
  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Icon name="home" className="mr-md" />
        <Icon name="arrowRight" className="mr-md" />
        <span>Yardım</span>
      </Container>
    </div>
  );
};

export default Breadcrumb;
