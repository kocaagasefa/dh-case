import classes from "./style.module.css";
import React from "react";
import Icon from "../../Common/Icon";
import Container from "../../Common/Container";

const Top = () => {
  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <div className={classes.contact}>
          <Icon name="telegram" className="mr-xl" />
          <Icon name="whatsapp" className="mr-sm" />
          <span> 0533 236 20 05</span>
          <Icon name="phone" className="mr-sm ml-xl" />
          <span>0212 236 74 41</span>
          <Icon name="envelope" className="ml-xl mr-sm" />
          <span>info@dopinghafiza.com</span>
        </div>
        <div className={classes.social}>
          <Icon name="facebook" className="mr-xl" />
          <Icon name="instagram" className="mr-xl" />
          <Icon name="twitter" className="mr-xl" />
          <Icon name="youtube" />
        </div>
      </Container>
    </div>
  );
};

export default Top;
