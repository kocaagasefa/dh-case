import React from "react";
import Container from "../../Common/Container";
import NavLink from "./NavLink";
import classes from "./style.module.css";

const Nav = () => {
  return (
    <div className={classes.wrapper}>
      <Container>
        <nav className={classes.nav}>
          <div className="logo">
            <img alt="logo" src="/images/logo.svg" />
          </div>
          <ul className={classes.navLinks}>
            <NavLink to="/" className="mr-lg">
              Eğitim Paketlerimiz
            </NavLink>
            <NavLink to="/" className="mr-lg">
              Örnek Videolar
            </NavLink>
            <NavLink to="/" className="mr-lg">
              Yorumlar & Başarılarımız
            </NavLink>
            <NavLink to="/" className="mr-lg" active>
              Yardım
            </NavLink>
            <NavLink to="/" className="mr-lg">
              Biz Kimiz
            </NavLink>
            <NavLink to="/">Bize Ulaşın</NavLink>
          </ul>
        </nav>
      </Container>
    </div>
  );
};

export default Nav;
