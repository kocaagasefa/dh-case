import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import classes from "./style.module.css";
const NavLink = (props) => {
  const classnames = cx(classes.navLink, {
    [classes.active]: props.active,
  });
  return (
    <li className={props.className}>
      <a className={classnames} href={props.to}>
        {props.children}
      </a>
    </li>
  );
};

NavLink.propTypes = {
  to: PropTypes.string,
  active: PropTypes.bool,
};

export default NavLink;
