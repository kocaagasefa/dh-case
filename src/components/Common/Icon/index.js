import React from "react";
import icons from "../../../constants/icons";
import cx from "classnames";
import classes from "./style.module.css";

const Icon = ({ name, className, ...props }) => {
  return (
    <img
      src={icons[name]}
      alt={name}
      {...props}
      className={cx(className, { [classes.button]: props.onClick })}
    />
  );
};

export default Icon;
