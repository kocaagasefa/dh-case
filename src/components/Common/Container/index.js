import React from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import classes from "./style.module.css";

const Container = (props) => {
  const classnames = cx(classes.container, props.className, {
    [classes.fullWidth]: props.fullWidth,
  });

  return <div className={classnames}>{props.children}</div>;
};

Container.propTypes = {
  fullWidth: PropTypes.bool,
};

Container.defaultProps = {
  fullWidth: false,
};
export default Container;
