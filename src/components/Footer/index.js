import React from "react";
import classes from "./style.module.css";

const links = [
  {
    icon: "/images/Facebook-f.svg",
    to: "/",
  },
  {
    icon: "/images/Instagram-f.svg",
    to: "/",
  },
  {
    icon: "/images/Twitter-f.svg",
    to: "/",
  },
  {
    icon: "/images/Youtube-f.svg",
    to: "/",
  },
  {
    icon: "/images/Telegram-f.svg",
    to: "/",
  },
  {
    icon: "/images/Bip-f.svg",
    to: "/",
  },
];

const Footer = () => {
  return (
    <div className={classes.container}>
      <img
        src="/images/left-ribon.svg"
        alt="ribon left"
        className={classes.ribonLeft}
      />

      <ul className={classes.links}>
        {links.map((link, i) => (
          <li key={i}>
            <a href={link.to}>
              <img src={link.icon} alt={link.name} />
            </a>
          </li>
        ))}
      </ul>
      <img
        src="/images/right-ribon.svg"
        alt="ribon right"
        className={classes.ribonRight}
      />
    </div>
  );
};

export default Footer;
