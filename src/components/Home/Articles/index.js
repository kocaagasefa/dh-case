import React from "react";
import articles from "../../../constants/articles";
import Container from "../../Common/Container";
import Icon from "../../Common/Icon";
import classes from "./style.module.css";

const Articles = () => {
  return (
    <Container>
      <div className={classes.wrapper}>
        {articles.map((category, i) => (
          <div key={i} className={classes.category}>
            <img src={category.image} alt={category.category} />
            <h3 className={classes.title}>{category.category}</h3>
            <ul className={classes.subCategory}>
              {category.subCategories.map((sCategory, i) => (
                <li key={i} style={{ marginBottom: 25 }}>
                  <a href="/" className={classes.subCatItem}>
                    {sCategory.title}
                  </a>
                </li>
              ))}
            </ul>
            <div className={classes.showAllWrapper}>
              <a href="/" className={classes.showAll}>
                Tümünü Göster
              </a>
              <Icon name="circledRightArrow" className="ml-sm" />
            </div>
          </div>
        ))}
      </div>
    </Container>
  );
};

export default Articles;
