import React from "react";
import Container from "../../Common/Container";
import Search from "../Search";
import classes from "./style.module.css";

const Banner = () => {
  return (
    <div
      className={classes.wrapper}
      style={{ backgroundImage: "url('/images/Banner.png')" }}
    >
      <Container>
        <p className={classes.titleMD}>Sorun Yaşıyorsanız</p>
        <p className={classes.titleXL}>Yardım için buradayız.</p>
        <Search />
      </Container>
      <img
        src="/images/right-ribon.svg"
        alt="ribon right"
        className={classes.ribonRight}
      />
      <img
        src="/images/left-ribon.svg"
        alt="ribon left"
        className={classes.ribonLeft}
      />
    </div>
  );
};

export default Banner;
