import React from "react";
import faq from "../../../constants/faq";
import Container from "../../Common/Container";
import Question from "./Question";
import classes from "./style.module.css";

const FAQ = () => {
  return (
    <Container className={classes.container}>
      <div className={classes.header}>
        <h2 className={classes.title}>Merak Edilen Sorular?</h2>
        <p className={classes.desc}>
          En çok merak edilen konuları bir araya topladık. 😇
        </p>
      </div>
      {faq.map((q, i) => (
        <Question {...q} key={i} />
      ))}
      <div className={classes.header}>
        <h2 className={classes.title}>
          Yararlı bulabileceğiniz konu başlıkları
        </h2>
        <p className={classes.desc}>
          Doping Hafıza Online Eğitim Platformu ile ilgili merak edilen konu
          başlıklarına aşağıdan ulaşabilirsiniz.
        </p>
      </div>
    </Container>
  );
};

export default FAQ;
