import React, { useState } from "react";
import { Collapse } from "react-collapse";
import cx from "classnames";
import Icon from "../../../Common/Icon";
import classes from "./style.module.css";

const Question = ({ question, answer }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={classes.wrapper}>
      <div className={classes.titleWrapper}>
        <h3 className={cx(classes.question, { [classes.open]: isOpen })}>
          {question}
        </h3>
        <Icon
          name={isOpen ? "minus" : "plus"}
          onClick={() => setIsOpen(!isOpen)}
        />
      </div>
      <Collapse isOpened={isOpen}>
        <div
          dangerouslySetInnerHTML={{ __html: answer }}
          className={classes.answer}
        />
      </Collapse>
    </div>
  );
};

export default Question;
