import React, { useMemo, useState } from "react";
import cx from "classnames";
import Icon from "../../Common/Icon";
import classes from "./style.module.css";
import result from "./dummyResults.json";
import SearchResult from "./SearchResult";
const Search = () => {
  const [text, setText] = useState("");

  const res = useMemo(() => {
    if (!text) return [];

    return result.filter((item) =>
      item.toLocaleLowerCase().includes(text.toLocaleLowerCase())
    );
  }, [text]);
  console.log("res", res);
  return (
    <div className={cx(classes.wrapper, { [classes.open]: text })}>
      <Icon name="search" />
      <input
        className={classes.input}
        onChange={(e) => setText(String(e.target.value))}
        value={text}
        placeholder="Yardım makalelerinde ara..."
      />
      {text && <SearchResult result={res} />}
    </div>
  );
};

export default Search;
