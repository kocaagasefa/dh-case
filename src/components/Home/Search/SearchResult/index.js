import React from "react";
import cx from "classnames";
import classes from "./style.module.css";

const SearchResult = ({ result = [] }) => {
  return (
    <div className={classes.container}>
      <ul className={classes.resultList}>
        {result.map((item, i) => (
          <li
            key={i}
            dangerouslySetInnerHTML={{ __html: item }}
            className={classes.resultItem}
          />
        ))}
        {!result[0] && (
          <li className={cx(classes.resultItem, classes.notFound)}>
            Sonuç bulunamadı!!!
          </li>
        )}
      </ul>
    </div>
  );
};

export default SearchResult;
