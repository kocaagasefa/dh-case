import React from "react";
import Container from "../../Common/Container";
import Icon from "../../Common/Icon";
import classes from "./style.module.css";

const Chatbot = () => {
  return (
    <Container>
      <div className={classes.container}>
        <div className={classes.iconContainer}>
          <Icon name="chat" />
        </div>
        <span className={classes.description}>
          Dilerseniz <strong>Doping Hafıza Uzmanlarımıza danışabilir,</strong>{" "}
          merak ettiklerinizi en hızlı şekilde cevaplayabiliriz.
        </span>
        <div className={classes.button}>Konuşma Başlat</div>
      </div>
    </Container>
  );
};

export default Chatbot;
