import React from "react";
import Breadcrumb from "../Breadcrumb";
import Footer from "../Footer";
import Nav from "../Header/Nav";
import Top from "../Header/Top";
import classes from "./style.module.css";

const Layout = (props) => {
  return (
    <div className={classes.container}>
      <Top />
      <Nav />
      <Breadcrumb />
      {props.children}
      <Footer />
    </div>
  );
};

export default Layout;
