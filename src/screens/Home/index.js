import React from "react";
import Articles from "../../components/Home/Articles";
import Banner from "../../components/Home/Banner";
import Chatbot from "../../components/Home/Chatbot";
import FAQ from "../../components/Home/FAQ";

const Home = () => {
  return (
    <div>
      <Banner />
      <FAQ />
      <Articles />
      <Chatbot />
    </div>
  );
};

export default Home;
