const icons = {
  telegram: "/images/Telegram.svg",
  whatsapp: "/images/Whatsapp.svg",
  phone: "/images/Phone.svg",
  envelope: "/images/Envelope.svg",
  facebook: "/images/Facebook.svg",
  instagram: "/images/Instagram.svg",
  twitter: "/images/Twitter.svg",
  youtube: "/images/Youtube.svg",
  arrowRight: "/images/arrow-right.svg",
  home: "/images/Home.svg",
  search: "/images/SearchArticle.svg",
  plus: "/images/Plus.svg",
  minus: "/images/Minus.svg",
  circledRightArrow: "/images/CircledRight.svg",
  chat: "/images/Chat.svg",
};

export default icons;
