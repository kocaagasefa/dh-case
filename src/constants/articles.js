const articles = [
  {
    category: "Genel Konular",
    image: "/images/Weirdo-Head.png",
    subCategories: [
      {
        title: "Doping Hafıza",
      },
      {
        title: "Hakkımızda",
      },
      {
        title: "Hafıza Teknikleri",
      },
      {
        title: "Reflekslerle Matematik",
      },
      {
        title: "Serüvenlerle Fen",
      },
    ],
  },
  {
    category: "Eğitim Paketlerimiz",
    image: "/images/Package.png",
    subCategories: [
      {
        title: "Hangi Paketi Seçmeliyim?",
      },
      {
        title: "Eğitim paketleri arasındaki farklar nelerdir",
      },
      {
        title: "Eğitim içerikleri",
      },
      {
        title: "Doping Hafıza neye benziyor?",
      },
    ],
  },
  {
    category: "Satın Alma Süreçleri",
    image: "/images/Basket.png",
    subCategories: [
      {
        title: "Ödeme seçenekleri",
      },
      {
        title: "Satın alırken neden kullanıcı bilgilerimi istiyorsunuz?",
      },
      {
        title: "Satın aldıktan sonra ne olacak?",
      },
      {
        title: "Satın alırken çıkan Dönem Seçimi nedir?",
      },
    ],
  },
  {
    category: "İptal ve İade",
    image: "/images/Refund.png",
    subCategories: [
      {
        title: "Mesafeli Satış Sözleşmesi",
      },
      {
        title: "İptal ve İade Şartları",
      },
      {
        title: "Sipariş iptal politikası",
      },
      {
        title: "Kişisel bilgilerin güvenliği",
      },
      {
        title: "KVKK Aydınlatma Metni",
      },
    ],
  },
];

export default articles;
