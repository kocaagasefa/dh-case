const faq = [
  {
    question: "Doping Hafıza nedir?",
    answer: `<p>Doping Hafıza TYT-AYT-LGS-KPSS ve DGS sınavlarına hazırlık aşamasında öğrencilerin çağı yakalayan online eğitim yöntemleri ve yapay zeka teknolojisini kullanarak etkili sonuçlar aldığı bir online eğitim platformudur. </p>
            <p> Doping Hafıza bünyesinde YKS, KPSS, LGS ve DGS'ye online hazırlık paketleri bulunmaktadır. Bu paketlerde Doping Hafıza; hafıza teknikleri ve akıl haritaları gibi öğretim enstrümanlarının yanı sıra yapay zeka destekli akıllı test paneli, çalışma programı desteği, infografikler, rehberlik videoları ve Reflekslerle Matematik ile Serüvenlerle Fen Bilimleri gibi sayısal dersler özelinde geliştirdiği konseptleri ile öğrenciler için doğru bir adres olma kimliğini göstermektedir.</p>
            <iframe title="vimeo-player" src="https://player.vimeo.com/video/38160323" width="640" height="360" frameborder="0" allowfullscreen></iframe></p>
            `,
  },
  {
    question:
      "Seneler alan eğitimleri saatlerle ifade edilen sürelerde veriyorsunuz. Bu nasıl oluyor?",
    answer:
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum.</p>",
  },
  {
    question:
      "Online eğitim ile bu derece ciddi bir sınava hazırlanmak doğru mu?",
    answer:
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum.</p>",
  },
  {
    question:
      "Eğitimcilerin ders hakimiyeti, anlatımı ve sundukları ders içerikleri ne ölçüde güvenilir?",
    answer:
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum.</p>",
  },
  {
    question:
      "Doping Hafıza’yı satın almadan neye benzediğini nasıl görebilirim?",
    answer:
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum.</p>",
  },
  {
    question:
      "Doping Hafıza’yı neden sadece 2 farklı cihazda kullanabiliyorum?",
    answer:
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum.</p>",
  },
];

export default faq;
